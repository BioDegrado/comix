!#/bin/sh

defaultDataPath="DefaultData"

user=
database=
host=
port=
pass=

# $1 : Filename with the values
# $2 : Table name
function insertInto {
	while read line; do
		echo "insert into public.$2 values('$line') on conflict do nothing;"
	done < "$defaultDataPath/$1"
}

echo "Insert the database user (default is postgres): "
read user; [ -z $user ] && user="postgres"

echo "Insert the database name (default is ComixDB):"
read database; [ -z $database ] && database="ComixDB"

echo "Insert the address of $database (default is localhost):"
read host; [ -z $host ] && host="localhost"

echo "Insert the port od $database (default is 42069):"
read port; [ -z $port ] && port="42069"

echo "Insert the password of $database:"
read -s pass

location="postgresql://$user:$pass@$host:$port/$database"

echo "Inserting nations..."
psql "$location" -c "$(insertInto 'Nation' 'nation')" 
echo "Inserting sexes..."
psql "$location" -c "$(insertInto 'Sex' 'sex')" 
echo "Inserting languages..."
psql "$location" -c "$(insertInto 'Language' 'language')" 
echo "Inserting genres..."
psql "$location" -c "$(insertInto 'Genre' 'genre')" 

echo "Inserting public group and user..."
psql "$location" -c "insert into public.group values('0', 'public', '2') on conflict do nothing;
insert into public.user(oid, group_oid) values('0','0') on conflict do nothing"

echo "Executed successfully!"
read host
