# Comix

This is a project for Advanced Software Engineering on The university of Sapienza.

This repository contains the project "working" (hopefully), and a paper describing it, and the development process.

## Initialization

The objective of Webratio is to make development, less dependent on technologies, so that the developer can focus on more "abstract stuff", instead of banging his head on the wall due to implementation problems!

Unfortunately, the documentation is not helping too much in understanding the "language", so a few design and implementation decisions, don't really follow the original intention of the Webratio team.

### The database initialization

So... We don't really know how to initialize the database server using only webratio, therefore we are using the usual SQL scripts to initialize the database. This brings the following problem: We are using the naming of the tables instead of the Entities. So if the mapping is not matching, then the scripts won't work. F U C K !
Moreover we are using the shell scripting for interfacing the user, hence on Windows alone shouldn't work either, unless using the WSL, or other tools!

```SHELL
./Scripts/InitDB.sh
```

To fill the database you have to start the InitDB.sh script!

> Apparently the script can be started directly from WebRatio! So just click on it on the project view!

## The Database

The database needs to be mapped!

By default we are using:

* IP: localhost
* Port: 42069
* DB: ComixDB
