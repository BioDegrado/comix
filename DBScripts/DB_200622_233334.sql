-- REL FK: BookNotificationSentToUser [rel19#role37]
alter table "public"."booknotification"   add constraint fk_user_booknotification foreign key ("oid") references "public"."booknotification" ("oid");


-- REL FK: BookNotificationAboutBook [rel20#role39]
alter table "public"."booknotification"   add constraint fk_comic_booknotification foreign key ("oid") references "public"."booknotification" ("oid");


-- BookDrawnByUser [rel21]
create view "public"."bookdrawnbyuser_view" as
select distinct AL1."oid" as "s_oid", AL2."user_oid" as "T_user_oid"
from  "public"."comic" AL1 
               inner join "public"."write" AL2 on AL1."comicseries_oid"=AL2."comicseries_oid";


