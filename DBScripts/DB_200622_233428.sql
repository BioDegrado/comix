-- BookNotification [ent13]
create table "public"."booknotification" (
   "date"  timestamp,
   "oid"  int4  not null,
   "message"  varchar(255),
  primary key ("oid")
);


-- BookNotificationUser [rel19]
alter table "public"."booknotification"  add column  "user_oid"  int4;
alter table "public"."booknotification"   add constraint fk_booknotification_user foreign key ("user_oid") references "public"."user" ("oid");


-- BookNotificationBook [rel20]
alter table "public"."booknotification"  add column  "comic_oid"  int4;
alter table "public"."booknotification"   add constraint fk_booknotification_comic foreign key ("comic_oid") references "public"."comic" ("oid");


-- BookDrawnByUser [rel21]
create view "public"."bookdrawnbyuser_view" as
select distinct AL1."oid" as "s_oid", AL2."user_oid" as "T_user_oid"
from  "public"."comic" AL1 
               inner join "public"."write" AL2 on AL1."comicseries_oid"=AL2."comicseries_oid";


