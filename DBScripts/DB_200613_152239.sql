-- Follow [rel16]
create table "public"."follow_2" (
   "user_oid"  int4 not null,
   "user_oid_2"  int4 not null,
  primary key ("user_oid", "user_oid_2")
);
alter table "public"."follow_2"   add constraint fk_follow_2_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."follow_2"   add constraint fk_follow_2_user_2 foreign key ("user_oid_2") references "public"."user" ("oid");


