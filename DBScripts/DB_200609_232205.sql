-- ComicSeries.PeopleFollowing [ent2#att22]
create view "public"."comicseries_peoplefollowing_vi" as
select AL1."oid" as "oid", count(distinct AL2."user_oid") as "der_attr"
from  "public"."comicseries" AL1 
               left outer join "public"."follow" AL2 on AL1."oid"=AL2."comicseries_oid"
group by AL1."oid";


