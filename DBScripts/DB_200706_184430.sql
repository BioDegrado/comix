-- NotificationAboutComicSerie [rel20]
alter table "public"."serienotification"  add column  "comicserie_oid_2"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_comicse_2 foreign key ("comicserie_oid_2") references "public"."comicserie" ("oid");


-- CommunityHasComicSerie [rel24]
alter table "public"."comicserie"  add column  "community_oid"  int4;
alter table "public"."comicserie"   add constraint fk_comicserie_community_2 foreign key ("community_oid") references "public"."community" ("oid");


