-- Community [ent11]
create table "public"."community" (
   "oid"  int4  not null,
  primary key ("oid")
);


-- Comment [ent4]
alter table "public"."comment"  add column  "type"  varchar(255);


-- Follow [rel16]
create table "public"."follow_2" (
   "user_oid"  int4 not null,
   "user_oid_2"  int4 not null,
  primary key ("user_oid", "user_oid_2")
);
alter table "public"."follow_2"   add constraint fk_follow_2_user_2 foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."follow_2"   add constraint fk_follow_2_user foreign key ("user_oid_2") references "public"."user" ("oid");


-- User_ComicSerie [rel24]
create table "public"."user_comicserie" (
   "user_oid"  int4 not null,
   "comicseries_oid"  int4 not null,
  primary key ("user_oid", "comicseries_oid")
);
alter table "public"."user_comicserie"   add constraint fk_user_comicserie_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."user_comicserie"   add constraint fk_user_comicserie_comicseries foreign key ("comicseries_oid") references "public"."comicseries" ("oid");


-- User_Book [rel25]
create table "public"."user_book" (
   "user_oid"  int4 not null,
   "comic_oid"  int4 not null,
  primary key ("user_oid", "comic_oid")
);
alter table "public"."user_book"   add constraint fk_user_book_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."user_book"   add constraint fk_user_book_comic foreign key ("comic_oid") references "public"."comic" ("oid");


-- Downvotes [rel6]
create table "public"."downvotes" (
   "user_oid"  int4 not null,
   "comment_oid"  int4 not null,
  primary key ("user_oid", "comment_oid")
);
alter table "public"."downvotes"   add constraint fk_downvotes_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."downvotes"   add constraint fk_downvotes_comment foreign key ("comment_oid") references "public"."comment" ("oid");


-- ComicSerie.points [ent2#att41]
create view "public"."hottestcomics_points_view" as
select AL1."oid" as "oid", AL2."der_attr" * 0.8 + AL1."views" * 0.2 as "der_attr"
from  "public"."comicseries" AL1 
               left outer join "public"."comicseries_peoplefollowing_vi" AL2 on AL1."oid"=AL2."oid";


