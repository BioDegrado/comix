-- Group [Group]
create table "public"."group" (
   "oid"  int4  not null,
   "groupname"  varchar(255),
  primary key ("oid")
);


-- Module [Module]
create table "public"."module" (
   "oid"  int4  not null,
   "moduleid"  varchar(255),
   "modulename"  varchar(255),
  primary key ("oid")
);


-- User [User]
create table "public"."user" (
   "oid"  int4  not null,
   "username"  varchar(255),
   "password"  varchar(255),
   "email"  varchar(255),
  primary key ("oid")
);


-- Comic [ent1]
create table "public"."comic" (
   "oid"  int4  not null,
   "title"  varchar(255),
   "number"  int4,
   "publishingdata"  time,
   "cover"  varchar(255),
  primary key ("oid")
);


-- ComicSeries [ent2]
create table "public"."comicseries" (
   "oid"  int4  not null,
   "seriestitle"  varchar(255),
   "cover"  varchar(255),
  primary key ("oid")
);


-- Page [ent3]
create table "public"."page" (
   "oid"  int4  not null,
   "num"  varchar(255),
   "pagelocation"  varchar(255),
  primary key ("oid")
);


-- Comment [ent4]
create table "public"."comment" (
   "oid"  int4  not null,
   "content"  text,
   "upvotes"  int4,
   "publishingdata"  varchar(255),
  primary key ("oid")
);


-- Group_DefaultModule [Group2DefaultModule_DefaultModule2Group]
alter table "public"."group"  add column  "module_oid"  int4;
alter table "public"."group"   add constraint fk_group_module foreign key ("module_oid") references "public"."module" ("oid");


-- Group_Module [Group2Module_Module2Group]
create table "public"."group_module" (
   "group_oid"  int4 not null,
   "module_oid"  int4 not null,
  primary key ("group_oid", "module_oid")
);
alter table "public"."group_module"   add constraint fk_group_module_group foreign key ("group_oid") references "public"."group" ("oid");
alter table "public"."group_module"   add constraint fk_group_module_module foreign key ("module_oid") references "public"."module" ("oid");


-- User_DefaultGroup [User2DefaultGroup_DefaultGroup2User]
alter table "public"."user"  add column  "group_oid"  int4;
alter table "public"."user"   add constraint fk_user_group foreign key ("group_oid") references "public"."group" ("oid");


-- User_Group [User2Group_Group2User]
create table "public"."user_group" (
   "user_oid"  int4 not null,
   "group_oid"  int4 not null,
  primary key ("user_oid", "group_oid")
);
alter table "public"."user_group"   add constraint fk_user_group_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."user_group"   add constraint fk_user_group_group foreign key ("group_oid") references "public"."group" ("oid");


-- Write [rel1]
create table "public"."write" (
   "user_oid"  int4 not null,
   "comicseries_oid"  int4 not null,
  primary key ("user_oid", "comicseries_oid")
);
alter table "public"."write"   add constraint fk_write_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."write"   add constraint fk_write_comicseries foreign key ("comicseries_oid") references "public"."comicseries" ("oid");


-- CommentOn [rel11]
alter table "public"."comment"  add column  "comic_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_comic foreign key ("comic_oid") references "public"."comic" ("oid");


-- CommentOn [rel12]
alter table "public"."comment"  add column  "page_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_page foreign key ("page_oid") references "public"."page" ("oid");


-- ComicSeries_Comic [rel2]
alter table "public"."comic"  add column  "comicseries_oid"  int4;
alter table "public"."comic"   add constraint fk_comic_comicseries foreign key ("comicseries_oid") references "public"."comicseries" ("oid");


-- Comic_Page [rel3]
alter table "public"."page"  add column  "comic_oid"  int4;
alter table "public"."page"   add constraint fk_page_comic foreign key ("comic_oid") references "public"."comic" ("oid");


-- Publish [rel4]
alter table "public"."comicseries"  add column  "user_oid"  int4;
alter table "public"."comicseries"   add constraint fk_comicseries_user foreign key ("user_oid") references "public"."user" ("oid");


-- Follow [rel5]
create table "public"."follow" (
   "user_oid"  int4 not null,
   "comicseries_oid"  int4 not null,
  primary key ("user_oid", "comicseries_oid")
);
alter table "public"."follow"   add constraint fk_follow_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."follow"   add constraint fk_follow_comicseries foreign key ("comicseries_oid") references "public"."comicseries" ("oid");


-- Reply [rel7]
alter table "public"."comment"  add column  "comment_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_comment foreign key ("comment_oid") references "public"."comment" ("oid");


-- User_Comment [rel8]
alter table "public"."comment"  add column  "user_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_user foreign key ("user_oid") references "public"."user" ("oid");


-- CommentOn [rel9]
alter table "public"."comment"  add column  "comicseries_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_comicseries foreign key ("comicseries_oid") references "public"."comicseries" ("oid");


