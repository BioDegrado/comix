-- Community [ent11]
alter table "public"."community"  add column  "name"  varchar(255);
alter table "public"."community"  add column  "date"  date;


-- ComicSerie [ent2]
alter table "public"."comicserie"  add column  "ispublic"  varchar(255);


-- Notification [ent6]
alter table "public"."serienotification"  add column  "type"  varchar(255);


-- User_Community [rel17]
create table "public"."user_community" (
   "user_oid"  int4 not null,
   "community_oid"  int4 not null,
  primary key ("user_oid", "community_oid")
);
alter table "public"."user_community"   add constraint fk_user_community_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."user_community"   add constraint fk_user_community_community foreign key ("community_oid") references "public"."community" ("oid");


-- Community_User [rel22]
alter table "public"."user"  add column  "community_oid"  int4;
alter table "public"."user"   add constraint fk_user_community foreign key ("community_oid") references "public"."community" ("oid");


-- Community_Comment [rel23]
create table "public"."community_comment" (
   "community_oid"  int4 not null,
   "comment_oid"  int4 not null,
  primary key ("community_oid", "comment_oid")
);
alter table "public"."community_comment"   add constraint fk_community_comment_community foreign key ("community_oid") references "public"."community" ("oid");
alter table "public"."community_comment"   add constraint fk_community_comment_comment foreign key ("comment_oid") references "public"."comment" ("oid");


-- Community_ComicSerie [rel24]
create table "public"."community_comicserie" (
   "community_oid"  int4 not null,
   "comicserie_oid"  int4 not null,
  primary key ("community_oid", "comicserie_oid")
);
alter table "public"."community_comicserie"   add constraint fk_community_comicserie_commun foreign key ("community_oid") references "public"."community" ("oid");
alter table "public"."community_comicserie"   add constraint fk_community_comicserie_comics foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- Community_User_2 [rel25]
create table "public"."community_user_2" (
   "community_oid"  int4 not null,
   "user_oid"  int4 not null,
  primary key ("community_oid", "user_oid")
);
alter table "public"."community_user_2"   add constraint fk_community_user_2_community foreign key ("community_oid") references "public"."community" ("oid");
alter table "public"."community_user_2"   add constraint fk_community_user_2_user foreign key ("user_oid") references "public"."user" ("oid");


-- NotificationCommunity [rel26]
alter table "public"."serienotification"  add column  "community_oid"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_community foreign key ("community_oid") references "public"."community" ("oid");


