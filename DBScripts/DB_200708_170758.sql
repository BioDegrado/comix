-- Notification [ent6]
create table "public"."notification" (
   "date"  timestamp,
   "oid"  int4  not null,
   "message"  varchar(255),
  primary key ("oid")
);


-- UserNotification [rel12]
alter table "public"."notification"  add column  "user_oid"  int4;
alter table "public"."notification"   add constraint fk_notification_user_2 foreign key ("user_oid") references "public"."user" ("oid");


-- Notification_User [rel19]
alter table "public"."notification"  add column  "user_oid_2"  int4;
alter table "public"."notification"   add constraint fk_notification_user foreign key ("user_oid_2") references "public"."user" ("oid");


-- GEN FK: SerieNotification --> Notification
alter table "public"."serienotification"   add constraint fk_serienotification_notificat foreign key () references "public"."notification" ();


-- GEN FK: BookNotification --> Notification
alter table "public"."booknotification"   add constraint fk_booknotification_notificati foreign key ("serienotification_oid") references "public"."notification" ("oid");


-- GEN FK: CommunityNotification --> Notification
alter table "public"."communitynotification"   add constraint fk_communitynotification_notif foreign key ("serienotification_oid") references "public"."notification" ("oid");


-- GEN FK: JoinRequest --> Notification
alter table "public"."joinrequest"   add constraint fk_joinrequest_notification foreign key ("serienotification_oid") references "public"."notification" ("oid");


-- GEN FK: InvitationRequest --> Notification
alter table "public"."invitationrequest"   add constraint fk_invitationrequest_notificat foreign key ("serienotification_oid") references "public"."notification" ("oid");


