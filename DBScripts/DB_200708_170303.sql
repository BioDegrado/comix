-- Notification [ent6]
alter table "public"."notification"  add column  "oid_2"  int4  not null;
alter table "public"."notification"  add column  "date_2"  timestamp;
alter table "public"."notification"  add column  "message_2"  varchar(255);


-- ComicSerie_SerieNotification [rel18]
alter table "public"."serienotification"  add column  "comicserie_oid_2"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_comicse_2 foreign key ("comicserie_oid_2") references "public"."comicserie" ("oid");


