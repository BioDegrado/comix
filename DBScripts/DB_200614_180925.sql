-- Community [ent11]
create table "public"."community" (
   "oid"  int4  not null,
  primary key ("oid")
);


-- Notification [ent6]
create table "public"."notification" (
   "oid"  int4  not null,
  primary key ("oid")
);


-- Follow [rel17]
create table "public"."follow_3" (
   "user_oid"  int4 not null,
   "comic_oid"  int4 not null,
  primary key ("user_oid", "comic_oid")
);
alter table "public"."follow_3"   add constraint fk_follow_3_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."follow_3"   add constraint fk_follow_3_comic foreign key ("comic_oid") references "public"."comic" ("oid");


-- User_Notification [rel18]
alter table "public"."notification"  add column  "user_oid"  int4;
alter table "public"."notification"   add constraint fk_notification_user foreign key ("user_oid") references "public"."user" ("oid");


