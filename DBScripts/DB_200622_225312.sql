-- User.NumFollowers [User#att48]
create view "public"."user_numfollowers_view" as
select AL1."oid" as "oid", count(distinct AL2."user_oid") as "der_attr"
from  "public"."user" AL1 
               left outer join "public"."follow_2" AL2 on AL1."oid"=AL2."user_oid_2"
group by AL1."oid";


