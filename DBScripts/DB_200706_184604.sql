-- ComicSerie [ent2]
create table "public"."comicserie" (
   "oid"  int4  not null,
   "seriestitle"  varchar(255),
   "cover"  varchar(255),
   "summary"  text,
   "views"  int4,
   "completed"  bool,
   "ispublic"  varchar(255),
  primary key ("oid")
);


-- Language [rel15]
alter table "public"."comicserie"  add column  "language_lang"  varchar(255);
alter table "public"."comicserie"   add constraint fk_comicserie_language foreign key ("language_lang") references "public"."language" ("lang");


-- NotificationAboutComicSerie [rel20]
alter table "public"."serienotification"  add column  "comicserie_oid_2"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_comicseri foreign key ("comicserie_oid_2") references "public"."comicserie" ("oid");


-- CommunityHasComicSerie [rel24]
alter table "public"."comicserie"  add column  "community_oid"  int4;
alter table "public"."comicserie"   add constraint fk_comicserie_community foreign key ("community_oid") references "public"."community" ("oid");


-- ComicSerie.numFollowers [ent2#att22]
create view "public"."comicseries_peoplefollowing_vi" as
select AL1."oid" as "oid", count(distinct AL2."user_oid") as "der_attr"
from  "public"."comicserie" AL1 
               left outer join "public"."follow_2" AL2 on AL1."oid"=AL2."comicserie_oid"
group by AL1."oid";


-- ComicSerie.numChapters [ent2#att24]
create view "public"."comicseries_numchapters_view" as
select AL1."oid" as "oid", count(distinct AL2."oid") as "der_attr"
from  "public"."comicserie" AL1 
               left outer join "public"."book" AL2 on AL1."oid"=AL2."comicserie_oid"
group by AL1."oid";


-- ComicSerie.points [ent2#att41]
create view "public"."hottestcomics_points_view" as
select AL1."oid" as "oid", AL2."der_attr" * 0.8 + AL1."views" * 0.2 as "der_attr"
from  "public"."comicserie" AL1 
               left outer join "public"."comicseries_peoplefollowing_vi" AL2 on AL1."oid"=AL2."oid";


