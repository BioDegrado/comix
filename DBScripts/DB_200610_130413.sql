-- Upvote [rel4]
create table "public"."upvote" (
   "user_oid"  int4 not null,
   "comment_oid"  int4 not null,
  primary key ("user_oid", "comment_oid")
);
alter table "public"."upvote"   add constraint fk_upvote_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."upvote"   add constraint fk_upvote_comment foreign key ("comment_oid") references "public"."comment" ("oid");


-- HottestComics.points [ent12#att26]
create view "public"."hottestcomics_points_view" as
select AL1."comicseries_oid" as "comicseries_oid", AL2."der_attr" * 0.8 + AL3."views" * 0.2 as "der_attr"
from  "public"."hottestcomics" AL1 
               left outer join "public"."comicseries_peoplefollowing_vi" AL2 on AL1."comicseries_oid"=AL2."oid"
               left outer join "public"."comicseries" AL3 on AL1."comicseries_oid"=AL3."oid";


-- ComicSeries.numChapters [ent2#att24]
create view "public"."comicseries_numchapters_view" as
select AL1."oid" as "oid", count(distinct AL2."oid") as "der_attr"
from  "public"."comicseries" AL1 
               left outer join "public"."comic" AL2 on AL1."oid"=AL2."comicseries_oid"
group by AL1."oid";


-- Comment.numUpvotes [ent4#att25]
create view "public"."comment_numupvotes_view" as
select AL1."oid" as "oid", count(distinct AL2."user_oid") as "der_attr"
from  "public"."comment" AL1 
               left outer join "public"."upvote" AL2 on AL1."oid"=AL2."comment_oid"
group by AL1."oid";


