-- UserDetails [ent6]
create table "public"."userdetails" (
   "oid"  int4  not null,
   "lastname"  varchar(255),
   "birthdate"  date,
   "profilepicture"  varchar(255),
   "sex"  varchar(255),
   "name"  varchar(255),
   "profiledescription"  text,
  primary key ("oid")
);


-- Nation [ent7]
create table "public"."nation" (
   "nation"  varchar(255)  not null,
  primary key ("nation")
);


-- Language [ent8]
create table "public"."language" (
   "lang"  varchar(255)  not null,
  primary key ("lang")
);


-- ComicSeries [ent2]
alter table "public"."comicseries"  add column  "description"  text;
alter table "public"."comicseries"  add column  "views"  int4;


-- Nationality [rel14]
alter table "public"."userdetails"  add column  "nation_nation"  varchar(255);
alter table "public"."userdetails"   add constraint fk_userdetails_nation foreign key ("nation_nation") references "public"."nation" ("nation");


-- Language [rel15]
alter table "public"."comicseries"  add column  "language_lang"  varchar(255);
alter table "public"."comicseries"   add constraint fk_comicseries_language foreign key ("language_lang") references "public"."language" ("lang");


-- UserDetails_User [rel16]
alter table "public"."user"  add column  "userdetails_oid"  int4;
alter table "public"."user"   add constraint fk_user_userdetails foreign key ("userdetails_oid") references "public"."userdetails" ("oid");


