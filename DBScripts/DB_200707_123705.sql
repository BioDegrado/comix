-- Community.admin [ent11#att40]
create view "public"."community_admin_view" as
select AL1."oid" as "oid", AL2."username" as "der_attr"
from  "public"."community" AL1 
               left outer join "public"."user" AL2 on AL1."oid"=AL2."community_oid";


