-- SerieNotification [ent6]
create table "public"."serienotification" (
   "date"  timestamp,
   "oid"  int4  not null,
   "message"  varchar(255),
  primary key ("oid")
);


-- User_SerieNotification [rel12]
alter table "public"."serienotification"  add column  "user_oid"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_user foreign key ("user_oid") references "public"."user" ("oid");


-- SerieNotification_ComicSerie [rel18]
alter table "public"."serienotification"  add column  "comicseries_oid"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_comicseri foreign key ("comicseries_oid") references "public"."comicseries" ("oid");


