-- BookNotification [ent13]
create table "public"."booknotification" (
   "date"  timestamp
);


-- SerieNotification [ent6]
create table "public"."serienotification" (
   "date"  timestamp,
   "attribute36"  varchar(255)
);


-- User_SerieNotification [rel12]
alter table "public"."serienotification"  add column  "user_oid"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_user foreign key ("user_oid") references "public"."user" ("oid");


-- SerieNotification_ComicSerie [rel18]
alter table "public"."serienotification"  add column  "comicseries_oid"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_comicseri foreign key ("comicseries_oid") references "public"."comicseries" ("oid");


-- Entity13_User [rel19]
alter table "public"."booknotification"  add column  "user_oid"  int4;
alter table "public"."booknotification"   add constraint fk_booknotification_user foreign key ("user_oid") references "public"."user" ("oid");


-- BookNotification_Book [rel20]
alter table "public"."booknotification"  add column  "comic_oid"  int4;
alter table "public"."booknotification"   add constraint fk_booknotification_comic foreign key ("comic_oid") references "public"."comic" ("oid");


