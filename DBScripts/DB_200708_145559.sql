-- SerieNotification [ent10]
create table "public"."serienotification_2" (
   "serienotification_oid"  int4  not null,
  primary key ("serienotification_oid")
);


-- BookNotification [ent12]
create table "public"."booknotification" (
   "serienotification_oid"  int4  not null,
  primary key ("serienotification_oid")
);


-- CommunityNotification [ent13]
create table "public"."communitynotification" (
   "serienotification_oid"  int4  not null,
  primary key ("serienotification_oid")
);


-- JoinRequest [ent14]
create table "public"."joinrequest" (
   "serienotification_oid"  int4  not null,
  primary key ("serienotification_oid")
);


-- InvitationRequest [ent15]
create table "public"."invitationrequest" (
   "serienotification_oid"  int4  not null,
  primary key ("serienotification_oid")
);


-- ComicSerie_SerieNotification [rel18]
alter table "public"."serienotification_2"  add column  "comicserie_oid"  int4;
alter table "public"."serienotification_2"   add constraint fk_serienotification_2_comicse foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- Notification_User [rel19]
alter table "public"."serienotification"  add column  "user_oid_2"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_user_2 foreign key ("user_oid_2") references "public"."user" ("oid");


-- BookNotification_Book [rel20]
alter table "public"."booknotification"  add column  "book_oid"  int4;
alter table "public"."booknotification"   add constraint fk_booknotification_book foreign key ("book_oid") references "public"."book" ("oid");


-- CommunityNotification_Community [rel28]
alter table "public"."communitynotification"  add column  "community_oid"  int4;
alter table "public"."communitynotification"   add constraint fk_communitynotification_commu foreign key ("community_oid") references "public"."community" ("oid");


-- JoinRequest_Community [rel29]
alter table "public"."joinrequest"  add column  "community_oid"  int4;
alter table "public"."joinrequest"   add constraint fk_joinrequest_community foreign key ("community_oid") references "public"."community" ("oid");


-- InvitationRequest_Community [rel30]
alter table "public"."invitationrequest"  add column  "community_oid"  int4;
alter table "public"."invitationrequest"   add constraint fk_invitationrequest_community foreign key ("community_oid") references "public"."community" ("oid");


-- GEN FK: SerieNotification --> Notification
alter table "public"."serienotification_2"   add constraint fk_serienotification_2_serieno foreign key ("serienotification_oid") references "public"."serienotification" ("oid");


-- GEN FK: BookNotification --> Notification
alter table "public"."booknotification"   add constraint fk_booknotification_serienotif foreign key ("serienotification_oid") references "public"."serienotification" ("oid");


-- GEN FK: CommunityNotification --> Notification
alter table "public"."communitynotification"   add constraint fk_communitynotification_serie foreign key ("serienotification_oid") references "public"."serienotification" ("oid");


-- GEN FK: JoinRequest --> Notification
alter table "public"."joinrequest"   add constraint fk_joinrequest_serienotificati foreign key ("serienotification_oid") references "public"."serienotification" ("oid");


-- GEN FK: InvitationRequest --> Notification
alter table "public"."invitationrequest"   add constraint fk_invitationrequest_serienoti foreign key ("serienotification_oid") references "public"."serienotification" ("oid");


