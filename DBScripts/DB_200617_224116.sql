-- Downvotes [rel6]
create table "public"."downvotes" (
   "user_oid"  int4 not null,
   "comment_oid"  int4 not null,
  primary key ("user_oid", "comment_oid")
);
alter table "public"."downvotes"   add constraint fk_downvotes_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."downvotes"   add constraint fk_downvotes_comment foreign key ("comment_oid") references "public"."comment" ("oid");


-- Comment.numUpvotes [ent4#att25]
drop view "public"."comment_numupvotes_view";
create view "public"."comment_numupvotes_view" as
select AL1."oid" as "oid", count(distinct AL2."user_oid") - count(distinct AL3."user_oid") as "der_attr"
from  "public"."comment" AL1 
               left outer join "public"."upvote" AL2 on AL1."oid"=AL2."comment_oid"
               left outer join "public"."downvotes" AL3 on AL1."oid"=AL3."comment_oid"
group by AL1."oid";


