-- Genre [ent5]
create table "public"."genre" (
   "name"  varchar(255)  not null,
  primary key ("name")
);


-- SeriesGenre [rel13]
create table "public"."seriesgenre" (
   "comicseries_oid"  int4 not null,
   "genre_name"  varchar(255) not null,
  primary key ("comicseries_oid", "genre_name")
);
alter table "public"."seriesgenre"   add constraint fk_seriesgenre_comicseries foreign key ("comicseries_oid") references "public"."comicseries" ("oid");
alter table "public"."seriesgenre"   add constraint fk_seriesgenre_genre foreign key ("genre_name") references "public"."genre" ("name");


-- SubGenre [rel6]
alter table "public"."genre"  add column  "genre_name"  varchar(255);
alter table "public"."genre"   add constraint fk_genre_genre foreign key ("genre_name") references "public"."genre" ("name");


