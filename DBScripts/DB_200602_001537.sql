-- Sex [ent9]
create table "public"."sex" (
   "sex"  varchar(255)  not null,
  primary key ("sex")
);


-- User [User]
alter table "public"."user"  add column  "birthdate"  date;
alter table "public"."user"  add column  "name"  varchar(255);
alter table "public"."user"  add column  "profiledescription"  text;
alter table "public"."user"  add column  "profilepicture"  varchar(255);
alter table "public"."user"  add column  "lastname"  varchar(255);


-- Nationality [rel10]
alter table "public"."user"  add column  "nation_nation"  varchar(255);
alter table "public"."user"   add constraint fk_user_nation foreign key ("nation_nation") references "public"."nation" ("nation");


-- Sex_User [rel14]
alter table "public"."user"  add column  "sex_sex"  varchar(255);
alter table "public"."user"   add constraint fk_user_sex foreign key ("sex_sex") references "public"."sex" ("sex");


