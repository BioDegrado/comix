-- Group [Group]
create table "public"."group" (
   "oid"  int4  not null,
   "groupname"  varchar(255),
  primary key ("oid")
);


-- Module [Module]
create table "public"."module" (
   "oid"  int4  not null,
   "moduleid"  varchar(255),
   "modulename"  varchar(255),
  primary key ("oid")
);


-- User [User]
create table "public"."user" (
   "oid"  int4  not null,
   "username"  varchar(255),
   "password"  varchar(255),
   "email"  varchar(255),
   "birthday"  date,
   "name"  varchar(255),
   "profiledescription"  text,
   "profilepicture"  varchar(255),
   "lastname"  varchar(255),
  primary key ("oid")
);


-- Book [ent1]
create table "public"."book" (
   "oid"  int4  not null,
   "title"  varchar(255),
   "number"  numeric(19, 2),
   "publishingdate"  timestamp,
   "cover"  varchar(255),
  primary key ("oid")
);


-- SerieNotification [ent10]
create table "public"."serienotification" (
   "notification_oid"  int4  not null,
  primary key ("notification_oid")
);


-- Community [ent11]
create table "public"."community" (
   "oid"  int4  not null,
   "name"  varchar(255),
   "date"  date,
   "description"  text,
  primary key ("oid")
);


-- BookNotification [ent12]
create table "public"."booknotification" (
   "notification_oid"  int4  not null,
  primary key ("notification_oid")
);


-- CommunityNotification [ent13]
create table "public"."communitynotification" (
   "notification_oid"  int4  not null,
  primary key ("notification_oid")
);


-- JoinRequest [ent14]
create table "public"."joinrequest" (
   "notification_oid"  int4  not null,
  primary key ("notification_oid")
);


-- InvitationRequest [ent15]
create table "public"."invitationrequest" (
   "notification_oid"  int4  not null,
  primary key ("notification_oid")
);


-- ComicSerie [ent2]
create table "public"."comicserie" (
   "oid"  int4  not null,
   "seriestitle"  varchar(255),
   "cover"  varchar(255),
   "summary"  text,
   "views"  int4,
   "completed"  bool,
   "ispublic"  varchar(255),
  primary key ("oid")
);


-- Page [ent3]
create table "public"."page" (
   "oid"  int4  not null,
   "page"  int4,
   "image"  varchar(255),
  primary key ("oid")
);


-- Comment [ent4]
create table "public"."comment" (
   "oid"  int4  not null,
   "content"  text,
   "publishingdata"  timestamp,
   "spoiler"  bool,
   "type"  varchar(255),
  primary key ("oid")
);


-- Genre [ent5]
create table "public"."genre" (
   "name"  varchar(255)  not null,
  primary key ("name")
);


-- Notification [ent6]
create table "public"."notification" (
   "date"  timestamp,
   "oid"  int4  not null,
   "message"  varchar(255),
  primary key ("oid")
);


-- Nation [ent7]
create table "public"."nation" (
   "nation"  varchar(255)  not null,
  primary key ("nation")
);


-- Language [ent8]
create table "public"."language" (
   "lang"  varchar(255)  not null,
  primary key ("lang")
);


-- Sex [ent9]
create table "public"."sex" (
   "sex"  varchar(255)  not null,
  primary key ("sex")
);


-- Group_DefaultModule [Group2DefaultModule_DefaultModule2Group]
alter table "public"."group"  add column  "module_oid"  int4;
alter table "public"."group"   add constraint fk_group_module foreign key ("module_oid") references "public"."module" ("oid");


-- Group_Module [Group2Module_Module2Group]
create table "public"."group_module" (
   "group_oid"  int4 not null,
   "module_oid"  int4 not null,
  primary key ("group_oid", "module_oid")
);
alter table "public"."group_module"   add constraint fk_group_module_group foreign key ("group_oid") references "public"."group" ("oid");
alter table "public"."group_module"   add constraint fk_group_module_module foreign key ("module_oid") references "public"."module" ("oid");


-- User_DefaultGroup [User2DefaultGroup_DefaultGroup2User]
alter table "public"."user"  add column  "group_oid"  int4;
alter table "public"."user"   add constraint fk_user_group foreign key ("group_oid") references "public"."group" ("oid");


-- User_Group [User2Group_Group2User]
create table "public"."user_group" (
   "user_oid"  int4 not null,
   "group_oid"  int4 not null,
  primary key ("user_oid", "group_oid")
);
alter table "public"."user_group"   add constraint fk_user_group_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."user_group"   add constraint fk_user_group_group foreign key ("group_oid") references "public"."group" ("oid");


-- Draw [rel1]
create table "public"."draw" (
   "user_oid"  int4 not null,
   "comicserie_oid"  int4 not null,
  primary key ("user_oid", "comicserie_oid")
);
alter table "public"."draw"   add constraint fk_draw_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."draw"   add constraint fk_draw_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- Nationality [rel10]
alter table "public"."user"  add column  "nation_nation"  varchar(255);
alter table "public"."user"   add constraint fk_user_nation foreign key ("nation_nation") references "public"."nation" ("nation");


-- CommentOn [rel11]
alter table "public"."comment"  add column  "book_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_book foreign key ("book_oid") references "public"."book" ("oid");


-- UserNotification [rel12]
alter table "public"."notification"  add column  "user_oid"  int4;
alter table "public"."notification"   add constraint fk_notification_user_2 foreign key ("user_oid") references "public"."user" ("oid");


-- SeriesGenre [rel13]
create table "public"."seriesgenre" (
   "comicserie_oid"  int4 not null,
   "genre_name"  varchar(255) not null,
  primary key ("comicserie_oid", "genre_name")
);
alter table "public"."seriesgenre"   add constraint fk_seriesgenre_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");
alter table "public"."seriesgenre"   add constraint fk_seriesgenre_genre foreign key ("genre_name") references "public"."genre" ("name");


-- Sex_User [rel14]
alter table "public"."user"  add column  "sex_sex"  varchar(255);
alter table "public"."user"   add constraint fk_user_sex foreign key ("sex_sex") references "public"."sex" ("sex");


-- Language [rel15]
alter table "public"."comicserie"  add column  "language_lang"  varchar(255);
alter table "public"."comicserie"   add constraint fk_comicserie_language foreign key ("language_lang") references "public"."language" ("lang");


-- Follow [rel16]
create table "public"."follow" (
   "user_oid"  int4 not null,
   "user_oid_2"  int4 not null,
  primary key ("user_oid", "user_oid_2")
);
alter table "public"."follow"   add constraint fk_follow_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."follow"   add constraint fk_follow_user_2 foreign key ("user_oid_2") references "public"."user" ("oid");


-- UserBeingBannedFromCommunity [rel17]
create table "public"."user_community" (
   "user_oid"  int4 not null,
   "community_oid"  int4 not null,
  primary key ("user_oid", "community_oid")
);
alter table "public"."user_community"   add constraint fk_user_community_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."user_community"   add constraint fk_user_community_community foreign key ("community_oid") references "public"."community" ("oid");


-- ComicSerie_SerieNotification [rel18]
alter table "public"."serienotification"  add column  "comicserie_oid"  int4;
alter table "public"."serienotification"   add constraint fk_serienotification_comicseri foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- Notification_User [rel19]
alter table "public"."notification"  add column  "user_oid_2"  int4;
alter table "public"."notification"   add constraint fk_notification_user foreign key ("user_oid_2") references "public"."user" ("oid");


-- ComicSeriesToComic [rel2]
alter table "public"."book"  add column  "comicserie_oid"  int4;
alter table "public"."book"   add constraint fk_book_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- BookNotification_Book [rel20]
alter table "public"."booknotification"  add column  "book_oid"  int4;
alter table "public"."booknotification"   add constraint fk_booknotification_book foreign key ("book_oid") references "public"."book" ("oid");


-- CommunityCreator [rel22]
alter table "public"."community"  add column  "oid"  int4;
alter table "public"."community"   add constraint fk_community_user foreign key ("oid") references "public"."user" ("oid");


-- Community_Comment [rel23]
alter table "public"."comment"  add column  "community_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_community foreign key ("community_oid") references "public"."community" ("oid");


-- CommunityHasComicSerie [rel24]
alter table "public"."comicserie"  add column  "community_oid"  int4;
alter table "public"."comicserie"   add constraint fk_comicserie_community foreign key ("community_oid") references "public"."community" ("oid");


-- CommunityHasUser [rel25]
create table "public"."community_user_2" (
   "community_oid"  int4 not null,
   "user_oid"  int4 not null,
  primary key ("community_oid", "user_oid")
);
alter table "public"."community_user_2"   add constraint fk_community_user_2_community foreign key ("community_oid") references "public"."community" ("oid");
alter table "public"."community_user_2"   add constraint fk_community_user_2_user foreign key ("user_oid") references "public"."user" ("oid");


-- CommunityNotification_Community [rel28]
alter table "public"."communitynotification"  add column  "community_oid"  int4;
alter table "public"."communitynotification"   add constraint fk_communitynotification_commu foreign key ("community_oid") references "public"."community" ("oid");


-- JoinRequest_Community [rel29]
alter table "public"."joinrequest"  add column  "community_oid"  int4;
alter table "public"."joinrequest"   add constraint fk_joinrequest_community foreign key ("community_oid") references "public"."community" ("oid");


-- Comic_Page [rel3]
alter table "public"."page"  add column  "book_oid"  int4;
alter table "public"."page"   add constraint fk_page_book foreign key ("book_oid") references "public"."book" ("oid");


-- InvitationRequest_Community [rel30]
alter table "public"."invitationrequest"  add column  "community_oid"  int4;
alter table "public"."invitationrequest"   add constraint fk_invitationrequest_community foreign key ("community_oid") references "public"."community" ("oid");


-- Upvote [rel4]
create table "public"."upvote" (
   "user_oid"  int4 not null,
   "comment_oid"  int4 not null,
  primary key ("user_oid", "comment_oid")
);
alter table "public"."upvote"   add constraint fk_upvote_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."upvote"   add constraint fk_upvote_comment foreign key ("comment_oid") references "public"."comment" ("oid");


-- Follow [rel5]
create table "public"."follow_2" (
   "user_oid"  int4 not null,
   "comicserie_oid"  int4 not null,
  primary key ("user_oid", "comicserie_oid")
);
alter table "public"."follow_2"   add constraint fk_follow_2_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."follow_2"   add constraint fk_follow_2_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- Downvotes [rel6]
create table "public"."downvotes" (
   "user_oid"  int4 not null,
   "comment_oid"  int4 not null,
  primary key ("user_oid", "comment_oid")
);
alter table "public"."downvotes"   add constraint fk_downvotes_user foreign key ("user_oid") references "public"."user" ("oid");
alter table "public"."downvotes"   add constraint fk_downvotes_comment foreign key ("comment_oid") references "public"."comment" ("oid");


-- Reply [rel7]
alter table "public"."comment"  add column  "comment_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_comment foreign key ("comment_oid") references "public"."comment" ("oid");


-- User_Comment [rel8]
alter table "public"."comment"  add column  "user_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_user foreign key ("user_oid") references "public"."user" ("oid");


-- CommentOn [rel9]
alter table "public"."comment"  add column  "comicserie_oid"  int4;
alter table "public"."comment"   add constraint fk_comment_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- GEN FK: SerieNotification --> Notification
alter table "public"."serienotification"   add constraint fk_serienotification_notificat foreign key ("notification_oid") references "public"."notification" ("oid");


-- GEN FK: BookNotification --> Notification
alter table "public"."booknotification"   add constraint fk_booknotification_notificati foreign key ("notification_oid") references "public"."notification" ("oid");


-- GEN FK: CommunityNotification --> Notification
alter table "public"."communitynotification"   add constraint fk_communitynotification_notif foreign key ("notification_oid") references "public"."notification" ("oid");


-- GEN FK: JoinRequest --> Notification
alter table "public"."joinrequest"   add constraint fk_joinrequest_notification foreign key ("notification_oid") references "public"."notification" ("oid");


-- GEN FK: InvitationRequest --> Notification
alter table "public"."invitationrequest"   add constraint fk_invitationrequest_notificat foreign key ("notification_oid") references "public"."notification" ("oid");


-- User.NumFollowers [User#att48]
create view "public"."user_numfollowers_view" as
select AL1."oid" as "oid", count(distinct AL2."user_oid") as "der_attr"
from  "public"."user" AL1 
               left outer join "public"."follow" AL2 on AL1."oid"=AL2."user_oid_2"
group by AL1."oid";


-- Community.admin [ent11#att40]
create view "public"."community_admin_view" as
select AL1."oid" as "oid", AL3."username" as "der_attr"
from  "public"."community" AL1 
               left outer join "public"."community" AL2 on AL1."oid"=AL2."community_oid"
               left outer join "public"."user" AL3 on AL2."oid"=AL3."oid";


-- ComicSerie.numFollowers [ent2#att22]
create view "public"."comicseries_peoplefollowing_vi" as
select AL1."oid" as "oid", count(distinct AL2."user_oid") as "der_attr"
from  "public"."comicserie" AL1 
               left outer join "public"."follow_2" AL2 on AL1."oid"=AL2."comicserie_oid"
group by AL1."oid";


-- ComicSerie.numChapters [ent2#att24]
create view "public"."comicseries_numchapters_view" as
select AL1."oid" as "oid", count(distinct AL2."oid") as "der_attr"
from  "public"."comicserie" AL1 
               left outer join "public"."book" AL2 on AL1."oid"=AL2."comicserie_oid"
group by AL1."oid";


-- ComicSerie.points [ent2#att41]
create view "public"."hottestcomics_points_view" as
select AL1."oid" as "oid", AL2."der_attr" * 0.8 + AL1."views" * 0.2 as "der_attr"
from  "public"."comicserie" AL1 
               left outer join "public"."comicseries_peoplefollowing_vi" AL2 on AL1."oid"=AL2."oid";


-- Comment.numUpvotes [ent4#att25]
create view "public"."comment_numupvotes_view" as
select AL1."oid" as "oid", count(distinct AL2."user_oid") - count(distinct AL3."user_oid") as "der_attr"
from  "public"."comment" AL1 
               left outer join "public"."upvote" AL2 on AL1."oid"=AL2."comment_oid"
               left outer join "public"."downvotes" AL3 on AL1."oid"=AL3."comment_oid"
group by AL1."oid";


-- BookDrawnByUser [rel21]
create view "public"."bookdrawnbyuser_view" as
select distinct AL1."oid" as "s_oid", AL2."user_oid" as "T_user_oid"
from  "public"."book" AL1 
               inner join "public"."draw" AL2 on AL1."comicserie_oid"=AL2."comicserie_oid";


