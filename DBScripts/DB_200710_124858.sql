-- PublicComicSerie [ent17]
create table "public"."publiccomicserie" (
   "comicserie_oid"  int4  not null,
  primary key ("comicserie_oid")
);


-- GEN FK: PublicComicSerie --> ComicSerie
alter table "public"."publiccomicserie"   add constraint fk_publiccomicserie_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


