-- PrivateComicSerie [ent16]
create table "public"."privatecomicserie" (
   "comicserie_oid"  int4  not null,
  primary key ("comicserie_oid")
);


-- PrivateComicSerie_Community [rel26]
alter table "public"."privatecomicserie"  add column  "community_oid"  int4;
alter table "public"."privatecomicserie"   add constraint fk_privatecomicserie_community foreign key ("community_oid") references "public"."community" ("oid");


-- GEN FK: PrivateComicSerie --> ComicSerie
alter table "public"."privatecomicserie"   add constraint fk_privatecomicserie_comicseri foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


