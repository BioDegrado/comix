-- HottestComics [ent12]
create table "public"."hottestcomics" (
   "comicseries_oid"  int4  not null,
   "oid"  int4  not null,
   "rank"  varchar(255),
  primary key ("comicseries_oid")
);


-- GEN FK: HottestComics --> ComicSeries
alter table "public"."hottestcomics"   add constraint fk_hottestcomics_comicseries foreign key ("comicseries_oid") references "public"."comicseries" ("oid");


