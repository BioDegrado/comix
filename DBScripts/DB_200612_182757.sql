-- ComicSerie.publishingDate [ent2#att26]
create view "public"."comicserie_publishingdate_view" as
select AL1."oid" as "oid", min(AL2."publishingdata") as "der_attr"
from  "public"."comicseries" AL1 
               left outer join "public"."comic" AL2 on AL1."oid"=AL2."comicseries_oid"
group by AL1."oid";


