-- REL FK: ComicSeriesBeingDrawnByUser [rel1#role2]
alter table "public"."draw"   add constraint fk_draw_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- REL FK: SerieHasGenre [rel13#role25]
alter table "public"."seriesgenre"   add constraint fk_seriesgenre_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- REL FK: ComicHasBook [rel2#role3]
alter table "public"."book"   add constraint fk_book_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- REL FK: ComicSerieOfNotification [rel20#role40]
alter table "public"."serienotification"   add constraint fk_serienotification_comicseri foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- REL FK: ComicSerieBeingFollowedByUser [rel5#role10]
alter table "public"."follow_2"   add constraint fk_follow_2_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


-- REL FK: ComicSeriesToComment [rel9#role18]
alter table "public"."comment"   add constraint fk_comment_comicserie foreign key ("comicserie_oid") references "public"."comicserie" ("oid");


