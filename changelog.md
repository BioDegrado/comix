# Version 0.01 -- The beginning --

* Registration, LogOut and LogIn implemented, with the associated webpages.
* Initial Domain Model implemented.

## To do

* Groups and protected area to fix.
* Comments to implement (module).
* The registration module to refactor.
* Everything else really.
* Drink a lot of water. Hydration is important!

# Version 0.011 -- First encounter --

* The registration action model has been heavily simplified, most of the logic is handled by the registration form, rather than the action module.
* Added a new Site View, the user one, still thinking if itâ€™s useful or not.

## To do

* Groups and protected area to fix.
* Comments to implement (module).
* The master page keeps crashing the site, probably null exception somewhere!
* The login needs a captcha in order to stop brute forcing!
* Everything else really.
* Drink a lot of water. Hydration is important!


## Update

* documentation template uploaded in the doc folder.
 
# Version 0.012 -- Level up --

* From now on the project will require the Bootstrap dependency and the Date function!

* The registration form is refactored, and updated:
* The sex field is now mandatory.
* Some labels are renamed, to something more sensible.
* The regex are now working, now last names and names cannot be: xXPussy_Destroyer69Xx. The username cannot contain special characters with the exception of some.
* The grouping, now is working. The user upon login will be redirected to the profile page.
* The User site view has been removed, since this wonâ€™t allow proper navigation. We will be using the protected area instead.
* The Bootstrap style has been applied. Now looking at the site wonâ€™t provoke urges to vomit.
* The date function unit has been added, the utility is dubious.

## To do

* Comments to implement (module).
* Need to model the profile view.
* Need to add a Community view.
* The notification system must be thinked about.
* The master page keeps crashing the site, probably null exception somewhere!
* The login needs a captcha in order to stop brute forcing!
* Everything else really.
* Drink a lot of water. Hydration is important!


# Version 0.03 -- Monster raid --

* The crashing has been resolved, apparently the cause was FullName.
* The edit profile is under construction.
* Now the default group is added automatically if there is none in the database.
* Some layout adjustments have been applied.
* Now the registration message will redirect to a dedicated page.
* The profile menu has been reordered.
* A create group module has been added: creating module has never been this easy!
* Drink a lot of water. Hydration is important!


## Update : 

* the documentation is updated . 

## To do

* first i need to find a solution to setup the postgres9 database in webratio , in order to run the project properly . 

# Update : 
* Data base (Sex table) configured and runs properly (on my system) . 
* The documentation for the â€œDomain specification and modelingâ€� got updated 

# Version 0.04 -- Rare Drop --

* Implemented the feature for adding a comic serie, this implies the following changes:
* Added 2 session entities: NewSerie, NewSerieGenre. These handle the temporary data during creation(Session entities).
* Added multiple pages for creation, since a dynamic form cannot be contained in a normal form.  
* New modules added:Create new serie, instantiate new genre, publish new serie and Delete new serie (The naming need adjustments).
* The edit profile feature is â€œcompleteâ€�, it only lacks the handling of communities.
* Refactoring of naming of multiple objects.
* The master page has been added, but itâ€™s mostly not working, yet.
* Fixed the profile module not being loaded, due to wrong parameters from the selector in the Register module.

# To do

* The validations need to be added to the creation of the serie.
* All the functionality relative to the creation of comics need to be added.
* The master page has a strange layout.
* Visibility is a poorly documented function, so tinkering requires a LOT of time.
* Subgenres can be dropped as a feature, tree traversing using DML or WebRatio could be too stressful.
* Everything else really.
* Drink a lot of water. Hydration is important!

# Version 0.045 -- Adamantium --

* Now, all the form are properly validated, with the exception of the registration form.
* All the function for handling comics are implemented, with the exception of the editing of genres and collaborators.
* This means that the pages for adding books and and pages are added.
* The action modules are added aswell.
* All the blobs(file type) are marked as â€œimageâ€�, so they are displayed as such on the pages.
* Minor refactoring are applied, in order to improve readability.
* The â€œHottestComicsâ€� list now display only 10 items.
* The list of comics now are divided into pages.
* The points attribute is added to the comicserie entity to calculate its popularity.
* The derived entity for hottest comic as been dropped.

## To do

* The master page has a strange layout.
* Visibility is a poorly documented function, so tinkering requires a LOT of time.
* Subgenres can be dropped as a feature, tree traversing using DML or WebRatio could be too stressful.
* The comments feature needs to be added.
* Editing page genre and collaborators of a serie.
* The book viewer.
* Modules for incrementing the views and notifications!
* Everything else really.
* Drink a lot of water. Hydration is important!

## Update :     
    Documentation updated to the point we are . ( needs edit ) 

# Version 0.1 -- Sky fortress --

* With the exception of notifications, and â€œfollowingâ€� books. All the functionality regarding book viewing and handling are implemented.
* The book viewer has modules to handle the reading of next or previous book chapters.
* The comment section is completely implemented, for the moment one can access it only on the homepage. This include all the functionalities:
* Posting comments.
* Upvoting them.
* Downvoting them.
* Replying them.
    Multiple modules are implemented to achieve this feature.
* The master page has a sensible layout.
* Now is possible to change the genres and collaborators of series.
* Sub genres are dropped.
* The search bar has been transformed into a module, in order to simplify maintainability.
* The creation of pages would create an exception, this has been fixed.
* The page layout of the book viewer has been modified so the images are now without list borders, and decorations.

# To do

* The comments need to be added to the book viewer and the series details.
* Modules for incrementing the views and notifications!
* The community can be worked on!
* Thatâ€™s it, FINALLY! and of course...
* Drink a lot of water. Hydration is important!


## update :
* The document got updated . 

# Version 0.2 -- Eternal companions --

* The following feature has been implemented.
* A user can follow /unfollow a serie.
* A user can potentially follow/unfollow an artist (user).
* The following function can be found on the series page.
* The unfollowing function is found on the profile page.
* The list of followed users and comics has been added to the Profile page.
* You can visit the page of the followed comic from the Profile page.
    The pages for interfacing with the profiles of other users are not implemented yet.
* Following books has been dropped, since it was useless.
* The comment section has been completely implemented!
* In order to distinguish between comments on the homepage and others, i introduced the attribute type to the comment. Apparently checking null values is too complicated for Webratio fuck.
* Organized a little better the module view.
* Emergency fix: The comment section has not set the correct â€œareaâ€�.

## To do

* Modules for incrementing the views and notifications!
* The community can be worked on!
* Thatâ€™s it, FINALLY! and of course...
* Drink a lot of water. Hydration is important!

## update : 

* the documentation got updated 


# Version 0.25 -- Grind --

* Fixed the following feature:
* Now the user cannot follow itself, as matter of fact the button will be blocked, what a narcissistic individual!
* The searching of profiles has been implemented, and working! The searching will display 2 lists; one for comics, and the other for users.
* The profile page will display the correct user that you are following!
* Now the public area has landmarks.
* The pages with the details of the profile has been implemented, with:
* The details of the user(attributes).
* The list of published series.

## To do

* Modules for incrementing the views and notifications!
* The community can be worked on!
* Drink a lot of water. Hydration is important!

# Version 0.3 -- The hero's call --

* Implemented the notification system.
* Added a validation rule for the number of the book in the edit book form.


## To do

* the changelog has been configured 
* Drink a lot of water. Hydration is important!

### update : changeLog 

1. downvote link doesn't work 2
1. "publish a comic series" link : step 3 doesn't terminate properly
1. can not register number 1 for book in a series if in other series this number has beeen used before in another serie
1. the view , completed , logic behind needs to get developed

# Version 0.31 -- Bug hunt --

* Fixed the bug of the downvote on the homepage: the action wasn't right.
* Fixed the publishing book when there were no followers: the OK Flow for lonely people is removed.
* Fixed the bug where cannot be more than 1 book with the same number even for different series: Now there is a key check for the validation, hence check only for the same book!
* Fixed the visibility of the Replying message.


## update
The pdf got updated

# Version 0.4 -- The Queen Bee --

* The community domain model has been added!
* Initial implementation of community IFML.
* The view count has been added!
* The deleting of the series now works again: there was an error on the database mapping on the relation community notification.
* The publication of series is just one form: We can send data from multiple form, with validation.
* The book edit page edit/create books using the same process for information and pages.

## Known Bugs

1. SOLVED: By providing the name of the community , during the comic creation phase, it is not published as a private one. Still can be seen in the "hottest comic view component".
1. DONT CARE: the book cover is a useless attribute, it is not shown or used any where (you can drop it if you want)
1. SOLVED: The user won't get notified when a new serie is published!
1. SOLVED: Generally speaking the dynamic forms are not working?!
1. The changing of admin is not working.

# Version 0.8 -- Ragnarok --

* Everything was discussed on the meeting, so this is a very brief list of changes:
	* The notification system has been changed: now it supports extended entities instead of "type". This means also that the old action definitions are also updated to accomodate the changes.
	* Added the 2 types of comic series: the public one and the private one: this implies that the publishing of the serie has to take into account which to publish.
	* All the modules and the interfaces relate to the communities are added: Create community, manage community, ban person, unban, send invitations/join etc...
	* The community contains only series and comments.
    
# update 
# the pre-final document has uploaded 
